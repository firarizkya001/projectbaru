import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, TouchableOpacity } from 'react-native';

export default class Logo extends Component {
    render(){
        return(
            <View style = {styles.container}>
                <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0)'
                 placeholder='Username' 
                 placeholderTextColor='#F61969'
                 />
                 <TextInput style={styles.inputBox} underlineColorAndroid='rgba(0,0,0,0.8)'
                 placeholder='Password'
                 secureTextEntry={true} 
                 placeholderTextColor='#F61969'
                 />
                 <TouchableOpacity style={styles.button}>
                     <Text style={styles.buttonText}>Sign In</Text>
                 </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
          flexGrow: 1,
          justifyContent: 'center',
          alignItems: 'center'
        },
    inputBox:{
        width:300,
        height:50,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        paddingHorizontal:15,
        fontSize: 16,
        color:'#F61969',
        marginVertical: 9
    },
    button:{
        width: 150,
        height:40,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        marginVertical:10,
        paddingVertical:13
    },
    buttonText: {
        fontSize:16,
        fontWeight:'500',
        color:'#F61969',
        textAlign:'center'
    },
});

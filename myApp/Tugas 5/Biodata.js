import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
            <Image source={require('./assets/background.png')}  style={styles.backgroundImage} />
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/profil.png')} />
                    </View>
                    <Text style={styles.name}>Fira Rizkya M</Text>
                </View>
                <View style={styles.nim}>
                    <Text style={styles.nim}>nim : 181351084</Text>
                </View>
                <View style={styles.kelas}>
                    <Text style={styles.kelas}>kelas : malam b </Text>
                </View>
                <View style={styles.prody}>
                    <Text style={styles.prody}>prody : Teknik Informatika</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.instagram}>Instagram : @firarizkya25 </Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        flex:1,
        width: 300
    },
    backgroundImage:{
    flex: 1,
    resizeMode : 'cover',
    position:'absolute',
    width:500,
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
    },
    name: {
        color: '#F61969',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    nim :{
      color: '#F61969',
      marginTop: 24,
      fontSize: 20,
      fontWeight: 'bold'
    },
    kelas:{
      color: '#F61969',
      marginTop: 24,
      fontSize: 20,
      fontWeight: 'bold'
    },
    prody:{
      color: '#F61969',
      marginTop: 24,
      fontSize: 20,
      fontWeight: 'bold'
    },
    instagram:{
      color: '#F61969',
      marginTop: 24,
      fontSize: 20,
      fontWeight: 'bold'
    }
});
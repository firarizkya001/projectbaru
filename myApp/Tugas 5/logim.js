import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar, Image } from 'react-native';

import Logo from './Logo';
import Form from './form';
export default class login extends Component {
    render() {
        return(
            <View style = {StyleSheet.container}>
            <Image source={require('./assets/background.png')}  style={styles.backgroundImage} />
                <Logo />
                <Form />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
          flex: 1,
          backgroundColor: '#445a64',
          alignItems: 'center',
          justifyContent: 'center'
        },
    backgroundImage:{
    position:'absolute',
    weidth:'430px',
    height:'667px',
    top:'-100px',
    left:'41px'
  }
});